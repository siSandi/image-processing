function [ g ] = localhisteq(f, m, n)

%If number of inputs is less than one, no input image. 
%Return error msg.
if nargin <1
    error('localhisteq: f is a required input')
end

%If number of inputs is less than two, m and n were not input and will be set to default value of 3
    if nargin<2 
        m=3;
        n=3; 
    end
 
%If number of inputs is less than 3, m will be checked to be greater than 3 and and odd number. 
%If true for both, n will be set to value of m.
        if nargin<3
            if m>=3&&mod(m,2)==1
            n=m; 
            else 
            error('localhisteq: input m has to be an odd integer greater than 3')
            end
        end

%If number of inputs is less than 4, m and n will be checked to be greater than 3 and odd numbers. 
%If true for all, localhisteq can continue. 
            if nargin<4
                if m>=3&&mod(m,2)==1&&n>=3&&mod(n,2)==1
                else 
                error('localhisteq: input m and n have to be odd integers greater than 3')
                end
            end

%Pad image according to inputs m and n using mirrored pixels            
r=(m-1)/2;
c=(n-1)/2;
fp = padarray(f,[r c], 'symmetric');

%Define output image to be the same size as input image
g=uint8(zeros(size(f,1), size(f,2)));

%Iterate through each element of PADDED image up until row and column sizes subtracted by m+1 and n+1 respectively. 
%This completes local histogram equalization for the all pixels within the original input image.  
%w is the defined filter window for spatial filtering and holds the values of a sub-neighborhood, sized m by n, of the padded image
%hw holds the histogram equalized values of window w
%gp holds the middle value of the window and is assigned to the output image g at the appropriate pixel location
%Note g is already the original input image size (no padding)

for i=1:size(fp,1)-m+1
    for j=1:size(fp,2)-n+1
        w=fp(i:i+m-1,j:j+n-1);
        hw=histeq(w);
        gp=hw(1+r,1+c);
        g(i,j)=gp;
    
    end
    
imshow(g)
        
end